# Plasma-Mobile Emulator

                   
### Introduction 
---
The idea of *Plasma-Mobile Emulator* is to make plasma mobile working inside desktop. Plasma-Mobile Emulator will be the solution for developing, testing and accessing plasma phone without having real phone.

### How to use it
---
1. Get source code from KDE git.
2. Build docker image using ```docker build -t plasma-emulator . ``` command.
3. To run the plasma mobile emulator use the script ```pm-emulator```.
