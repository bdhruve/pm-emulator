FROM ubuntu:xenial
RUN apt update
RUN apt install -y wget software-properties-common 

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 444DABCF3667D0283F894EDDE6D4736255751E5D
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E47F5011FA60FC1DEBB1998933056FA14AD3A421

RUN add-apt-repository 'http://archive.neon.kde.org/dev/unstable xenial main'

RUN apt-get -y update && apt-get install -y kwin-wayland kwin-wayland-backend-x11 kwin-wayland-backend-wayland
RUN apt-get install -y libgl1-mesa-glx libgl1-mesa-dri
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    chown ${uid}:${gid} -R /home/developer
    
RUN gpasswd -a developer video
RUN apt-get install -y dbus-x11 kirigami-gallery kpackagelauncherqml

USER developer
ENV HOME /home/developer
ENTRYPOINT ["dbus-launch", "--exit-with-session", "kpackagelauncherqml", "-a"]
